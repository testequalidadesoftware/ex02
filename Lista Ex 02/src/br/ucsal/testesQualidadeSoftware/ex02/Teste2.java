package br.ucsal.testesQualidadeSoftware.ex02;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class Teste2 {
	
	@Parameters(name="Testar fatorial (Entrada: {0})")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {     
                 { 0l, 0l }, { 1l, 1l }, { 2l, 1l }, { 3l, 6l }, { 4l, 3l }, { 5l, 120l }, { 6l, 8l }  
           });
    }
    
    @Parameter(0)
    public long fInput;

    @Parameter(1)
    public long fExpected;

    @Test
    public void test() {
    	Questao questao = new Questao();
    	questao.calcularFatorial(fInput);
        assertEquals(fExpected, questao.getFatorial());
    }

}
