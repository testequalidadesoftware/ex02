package br.ucsal.testesQualidadeSoftware.ex02;

import java.util.Random;

public class Questao {
	private long fatorial;
	
	public int numeroRandomico() {
		Random r = new Random();
		int n = r.nextInt(99) + 1;
		return n;
	}
	
	public long getFatorial() {
		return this.fatorial;
	}
	
	public void calcularFatorial(long valor) {
		long result = valor;

		for(long x = valor - 1; x > 0; x--) {
			result*= x;
		}
		
		this.fatorial = result;
	}
	
	public String printFatorial(int value, int result) {
		String message = String.format("Fatorial de %s � %s", value, result);

		System.out.println(message);
		return message;
	}
}
