package br.ucsal.testesQualidadeSoftware.ex02;

import junit.framework.*;

public class Teste extends TestCase {
	public void testCalcularFatorial1() {
		Questao questao = new Questao();

		questao.calcularFatorial(0);
		assertEquals(0, questao.getFatorial());
	}

	public void testCalcularFatorial2() {
		Questao questao = new Questao();

		questao.calcularFatorial(8);
		assertEquals(40320, questao.getFatorial());
	}
	
	public void testCalcularFatorial3() {
		Questao questao = new Questao();

		questao.calcularFatorial(1);
		assertEquals(1, questao.getFatorial());
	}
	
	public void testCalcularFatorial4() {
		Questao questao = new Questao();

		questao.calcularFatorial(-8);
		assertEquals(-8, questao.getFatorial());
	}
	
	public void testCalcularFatorial5() {
		Questao questao = new Questao();

		questao.calcularFatorial(-1);
		assertEquals(-1, questao.getFatorial());
	}
	
	public void testCalcularFatorial6() {
		Questao questao = new Questao();

		questao.calcularFatorial(14);
		assertEquals(87178291200l, questao.getFatorial());
	}
	
	public void testNumeroRandomico1() {
		Questao questao = new Questao();
		int numero = questao.numeroRandomico();

		assertTrue(numero > 0);
	}
	
	public void testNumeroRandomico2() {
		Questao questao = new Questao();
		int numero = questao.numeroRandomico();

		assertTrue(numero < 100);
	}
	
	public void testprintFatorial1() {
		Questao questao = new Questao();
		String actual = questao.printFatorial(0, 0);

		assertEquals("Fatorial de 0 � 0", actual);
	}
}
